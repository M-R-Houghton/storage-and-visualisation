# Generator
This program handles the *generation* and *connecting* of fibre networks.

## How to use
Firstly, a `Box` must be created. This is a cube which contains the fibre network. A
box can either be 2-dimensional or 3-dimensional.

Secondly, a fibre network must be generated. This is done by creating an instance of `FibreGenerator`, e.g. a `RandomFibreGenerator` which generates a network of fibres of fixed length and random orientation.

The fibre network must then be connected in some way by using a `Connector`.  The `ShortestDistanceConnector` class connects two fibres if the shortest distance between the two fibres is less than some given distance.

This data is then written to a file using a `BoxWriter`. The `PlaintextBoxWriter` class writes the data in a simple plaintext format.

The modular nature of the codebase allows any of the above steps to be easily modified. Below is an example of how to generate a random fibre network, connect the fibres based on the distance to other fibres, and then writes the data to a simple plaintext file format.

```cpp
#include "Box.hpp"
#include "PlaintextBoxWriter.hpp"
#include "RandomFibreGenerator.hpp"
#include "ShortestDistanceConnector.hpp"

#include <memory>

int main()
{
    RandomFibreGenerator generator;
    PlaintextBoxWriter writer;
    ShortestDistanceConnector connector;
    const float connectDistance = 3.f;
    const unsigned width = 10, height = 10, depth = 10, numFibres = 50;

    Box box(width, height, depth);              // Create the box (3-dimensional).
    generator.generate(box, numFibres);         // Generate some random fibres.
    connector.connect(box, connectDistance);    // Connect fibres if the distance between two fibres is
                                                // less than or equal to connectDistance.
    writer.write(box, "myfile.dat");            // Write the data to a file called myfile.dat.

    return 0;
}
```

## 2D vs 3D
It's extremely simple to choose whether you want to work in 2D or 3D. It's all
handled behind the scenes, just specify the dimensions of the box. If no depth
is specified, everything is handled in 2D. The visualiser picks up on this and
acts accordingly. 

## Building
```
make
```
The `Makefile` is designed to work for building on OS X and Linux. This produces an executable called `generator.out`. Using the `-j` flag may improve compile times by allowing make to use multithreading.

### ARC2
The generator makes use of many C++11 features, therefore in order to compile the project on ARC2 the following command must be executed before compilation in order to switch from the intel compiler to the latest available GNU compiler
which supports these C++11 features.
```
module switch intel gnu/4.9.1
```

##  Running
The program is executed like so:
```
./generator.out myfile.dat
```
This will produce two files, `myfile.dat` containing data about the generated fibre network, and `myfile.dat.con` which contains the same fibre network but with additional data representing the connections made in the fibre network. These files can then be passed into the visualiser program to visualise the fibre network.

### Performance
In its current state, the generator takes ~8 seconds to generate a 10x10x10 network of 20,000 fibres on a MacBook Pro (Retina, 13-inch, Early 2015).
This running time can be expected to increase quadratically with the number of fibres.