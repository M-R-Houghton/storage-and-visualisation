#ifndef CONNECTOR_HPP
#define CONNECTOR_HPP

#include "Box.hpp"

/**
 * \brief An abstract base class that handles the connecting of fibres in a box.
 */
class Connector
{
    public:
        /**
         * \brief Connects fibres together in some way.
         *
         * \param b The box containing the fibres.
         * \param epsilon The maximum distance between two fibres that can be connected.
         * \param connectingFibreRadius The radius of the new connecting fibre.
         */
        virtual void connect(Box& b, float epsilon, float connectingFibreRadius) = 0;
};

#endif
