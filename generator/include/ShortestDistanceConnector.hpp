#ifndef SHORTEST_CONNNECTOR_HPP
#define SHORTEST_CONNNECTOR_HPP

#include "Connector.hpp"

/**
 * \brief Connects two fibres if they are separated by a certain distance.
 */
class ShortestDistanceConnector : public Connector
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared by \c Connector.
         *
         * \param b The box containing the fibres.
         * \param epsilon The maximum distance between two fibres that can be connected.
         * \param connectingFibreRadius The radius of the new connecting fibre.
         */
        void connect(Box& b, float epsilon, float connectingFibreRadius);

        /**
         * \brief A concrete implementation of the pure virtual function declared by \c Connector.
         *
         * \param b The box containing the fibres.
         * \param epsilon The maximum distance between two fibres that can be connected.
         * \param connectingFibreRadius The radius of the new connecting fibre.
         *
         * \note This is a naive algorithm and so is very slow for large networks. This
         * should only be used for testing, \c connect() should be used in practice.
         */
        void connectSLOW(Box& b, float epsilon, float connectingFibreRadius);
};

#endif
