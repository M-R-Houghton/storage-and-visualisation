#ifndef RANDOM_FIBRE_GENERATOR_HPP
#define RANDOM_FIBRE_GENERATOR_HPP

#include "FibreGenerator.hpp"

#include <glm/glm.hpp>

#include <random>

/**
 * \brief Generates a fibre network where the start/end points of the fibres
 * are random.
 */
class RandomFibreGenerator : public FibreGenerator
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared
         * by \c FibreGenerator.
         *
         * \param box The box containing the generated fibre network.
         * \param num The number of fibres to generate.
         * \param length The length of the fibres.
         * \param radius The radius of the fibres.
         */
        void generate(Box& box, unsigned num, float length, float radius);

    private:
        static std::random_device rd;
        static std::default_random_engine generator;
        static std::uniform_real_distribution<float> distribution;

        /**
         * \brief Choose a random point within the boundaries of the box.
         * 
         * \param box The box that the randomly generated point should be contained in.
         *
         * \return A random point within the boundaries of \c box.
         */
        glm::vec3 randomPointWithinBox(const Box& box) const;

        /**
         * \brief Choose a random direction in 2D/3D scpae.
         * 
         * \param box The box that the randomly generated unit vector should be contained in.
         *
         * \return A unit vector representing a random direction.
         */
        glm::vec3 randomUnitVector(const Box& box) const;

        /**
         * \brief Generate a random 2D unit vector.
         * \return A random 2D unit vector.
         */
        glm::vec3 random2DVector() const;

        /**
         * \brief Generate a random 3D unit vector.
         * \return A random 3D unit vector.
         */
        glm::vec3 random3DVector() const;
};

#endif
