#include "CellList.hpp"

CellList::CellList(float width, float height, float depth, float size)
    : cellSize(size),
      numCellsX(std::ceil(width / size)),
      numCellsY(std::ceil(height / size)),
      numCellsZ(depth == 0 ? 1 : std::ceil(depth / size))
{
    // Initialize the 3-dimensional vector with the correct number of elements.
    cells.resize(numCellsX);
    for (unsigned x = 0; x < numCellsX; ++x)
    {
        cells[x].resize(numCellsY);
        for (unsigned y = 0; y < numCellsY; ++y)
        {
            cells[x][y].resize(numCellsZ);
        }
    }
}

void CellList::insert(Fibre& f)
{
    glm::vec3 midpoint = (f.start + f.end) / 2.f;

    // Ensure that we don't try inserting fibres past the boundaries of the vector.
    auto clampIndex = [&](int index, int endArray) -> int {
        if (index < 0) return 0;
        else if (index >= endArray) return endArray - 1;
        else return index;
    };

    int xIndex = clampIndex(midpoint.x / cellSize, numCellsX);
    int yIndex = clampIndex(midpoint.y / cellSize, numCellsY);
    int zIndex = clampIndex(midpoint.z / cellSize, numCellsZ);

    cells[xIndex][yIndex][zIndex].push_back(f);
}

unsigned CellList::getNumCellsX() const
{
    return numCellsX;
}

unsigned CellList::getNumCellsY() const
{
    return numCellsY;
}
unsigned CellList::getNumCellsZ() const
{
    return numCellsZ;
}

bool CellList::isIndexValid(unsigned x, unsigned y, unsigned z) const
{
    return x < numCellsX &&
           y < numCellsY &&
           z < numCellsZ;
}

std::vector<Fibre>& CellList::getCell(unsigned x, unsigned y, unsigned z)
{
    if (isIndexValid(x, y, z))
        return cells[x][y][z];
    else
        return emptyVector;
}

std::vector<Fibre> CellList::getCells(std::vector<glm::vec3> indices)
{
    std::vector<Fibre> fibres;

    for (auto index : indices)
    {
        if (isIndexValid(index.x, index.y, index.z))
        {
            auto& fibresToAdd = getCell(index.x, index.y, index.z);
            fibres.insert(fibres.end(), fibresToAdd.begin(), fibresToAdd.end());
        }
    }

    return fibres;
}

std::ostream& operator<<(std::ostream& out, const CellList& cellList)
{
    for (unsigned x = 0; x < cellList.numCellsX; ++x)
    {
        for (unsigned y = 0; y < cellList.numCellsY; ++y)
        {
            for (unsigned z = 0; z < cellList.numCellsZ; ++z)
            {
                auto& fibres = cellList.cells[x][y][z];

                out << "Cell (" << x << "," << y << "," << z << ") [" << fibres.size() << " fibres]\n";
                
                for (auto& f : fibres)
                    out << "  " << f << std::endl;
            }
        }
    }

    return out;
}
