#ifndef PNG_SCREEN_GRABBER_HPP
#define PNG_SCREEN_GRABBER_HPP

#include "ScreenGrabber.hpp"

/**
 * \brief Handles writing the pixels on the screen to a png file.
 */
class PNGScreenGrabber : public ScreenGrabber
{
    public:
        /**
         * \brief Default constructor. The prefix of the filenames is the default prefix.
         */
        PNGScreenGrabber();

        /**
         * \brief Construct a \c PNGScreenGrabber instance which uses the given prefix.
         * \param prefix The prefix for output filenames.
         */
        PNGScreenGrabber(const std::string& prefix);

        /**
         * \brief Writes the pixels on the screen to a png file.
         */
        void grab();
};

#endif
