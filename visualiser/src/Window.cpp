#include "Window.hpp"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

Window::Window(int width, int height, const std::string& title)
{
    int argc = 1;
    char* argv = (char*)" ";

    glutInit(&argc, &argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutCreateWindow(title.c_str());
}

void Window::start() const
{
    glutMainLoop();
}

void Window::initGL() const
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    glMatrixMode(GL_PROJECTION);
    gluPerspective( /* field of view in degree */ 40.0,
            /* aspect ratio */ 1.0,
            /* Z near */ 1.0, /* Z far */ 10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    gluLookAt(0.0, 0.0, 5.0,  /* eye is at (0,0,5) */
              0.0, 0.0, 0.0,  /* center is at (0,0,0) */
              0.0, 1.0, 0.0); /* up is in positive Y direction */

    glTranslatef(0.0, 0.0, -1.0);
    glPopMatrix();
}

void Window::setDisplayCallback(DisplayCallback* display)
{
    this->display = display;
    glutDisplayFunc(this->display);
}

void Window::setKeyboardCallback(KeyboardCallback* keyboard)
{
    this->keyboard = keyboard;
    glutKeyboardFunc(this->keyboard);
}

void Window::setSpecialKeyboardCallback(SpecialKeyboardCallback* specialKeyboard)
{
    this->specialKeyboard = specialKeyboard;
    glutSpecialFunc(this->specialKeyboard);
}

void Window::setMotionCallback(MotionCallback* motion)
{
    this->motion = motion;
    glutMotionFunc(this->motion);
}

void Window::setIdleCallback(IdleCallback* idle)
{
    this->idle = idle;
    glutIdleFunc(this->idle);
}

void Window::setMouseCallback(MouseCallback* mouse)
{
    this->mouse = mouse;
    glutMouseFunc(this->mouse);
}
