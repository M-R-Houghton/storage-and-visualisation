#include "Box.hpp"
#include "Window.hpp"
#include "GLInclude.hpp"
#include "BoxRenderer.hpp"
#include "MyBoxRenderer.hpp"
#include "FibreRenderer.hpp"
#include "MyFibreRenderer.hpp"
#include "PNGScreenGrabber.hpp"
#include "PlaintextBoxReader.hpp"

#include <random>
#include <iostream>

std::unique_ptr<Box> box;
std::unique_ptr<BoxRenderer> boxRenderer;
std::unique_ptr<FibreRenderer> fibreRenderer;
std::unique_ptr<ScreenGrabber> screenGrabber;

int angleX = 0;
int angleY = 0;

bool applyRotation = true;
const float ROTATION_FACTOR = 0.25f;

const int WINDOW_WIDTH = 600;
const int WINDOW_HEIGHT = 600;
const std::string WINDOW_TITLE = "Visualiser";

const unsigned char SCREENSHOT_KEY = 's';
const unsigned char ZOOM_IN_KEY = ']';
const unsigned char ZOOM_OUT_KEY = '[';

float zoomFactor = 1.f;
const float ZOOM_DELTA = 0.1f;
const float ZOOM_MIN = 0.2f;
const float ZOOM_MAX = 4.f;

float xDisplacement = 0.f;
float yDisplacement = 0.f;
const float DISPLACEMENT_DELTA = 0.05f;

void display(void)
{
    glClearColor(0.15f, 0.15f, 0.15f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.);

    glScalef(zoomFactor, zoomFactor, zoomFactor);
    glTranslatef(-xDisplacement, -yDisplacement, 0.f);
    glRotatef(angleX, 1.0, 0.0, 0.0);
    glRotatef(angleY, 0.0, 1.0, 0.0);

    boxRenderer->render(*box);
    fibreRenderer->render(*box);

    glPopMatrix();
    glutSwapBuffers();
}

void idle()
{
    display();
    glutPostRedisplay();
}

void motion(int x, int y)
{
    static int lastX = x;
    static int lastY = y;

    if (box->getDepth() != 0.f && applyRotation)
    {
        int deltaX = x - lastX;
        int deltaY = y - lastY;

        angleX += deltaY * ROTATION_FACTOR;
        angleY += deltaX * ROTATION_FACTOR;
    }

    lastX = x;
    lastY = y;

    applyRotation = true;
}

void mouse(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
        applyRotation = false;
    }
}

void specialKeyboard(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_UP:
            yDisplacement += DISPLACEMENT_DELTA;
            break;
        case GLUT_KEY_DOWN:
            yDisplacement -= DISPLACEMENT_DELTA;
            break;
        case GLUT_KEY_LEFT:
            xDisplacement -= DISPLACEMENT_DELTA;
            break;
        case GLUT_KEY_RIGHT:
            xDisplacement += DISPLACEMENT_DELTA;
            break;
        default:
            break;
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case SCREENSHOT_KEY:
            screenGrabber->grab();
            break;
        case ZOOM_IN_KEY:
            zoomFactor = (zoomFactor + ZOOM_DELTA >= ZOOM_MAX ? ZOOM_MAX : zoomFactor + ZOOM_DELTA);
            break;
        case ZOOM_OUT_KEY:
            zoomFactor = (zoomFactor - ZOOM_DELTA <= ZOOM_MIN ? ZOOM_MIN : zoomFactor - ZOOM_DELTA);
            break;
        default:
            break;
    }
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " inputfile" << std::endl;
        return 1;
    }

    Window window(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE + " - " + argv[1]);
    window.setDisplayCallback(display);
    window.setIdleCallback(idle);
    window.setMotionCallback(motion);
    window.setMouseCallback(mouse);
    window.setKeyboardCallback(keyboard);
    window.setSpecialKeyboardCallback(specialKeyboard);
    window.initGL();

    box = PlaintextBoxReader().read(std::string(argv[1]));

    boxRenderer = std::unique_ptr<BoxRenderer>(new MyBoxRenderer);
    fibreRenderer = std::unique_ptr<FibreRenderer>(new MyFibreRenderer);
    screenGrabber = std::unique_ptr<ScreenGrabber>(new PNGScreenGrabber);

    window.start();
    return 0;
}
